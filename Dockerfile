FROM node:latest

RUN set -x \
&& apt-get update && apt-get install -y \
curl \
wget \
mc \
libnss3 \
libxss1 \
libasound2 \
libatk-bridge2.0-0 \
libgtk-3-0 \
libdrm2 \
libgbm-dev

#FINAL
WORKDIR /var/app

ENV TERM=xterm-color

COPY run.sh /usr/local/bin/
ENTRYPOINT ["run.sh"]
CMD [ "node", "build/index.js" ]