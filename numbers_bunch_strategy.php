<?php

class Strategy
{

    /**
     * @var int[]
     */
    public $parsedNumbers;

    public $depositWin = 0;

    /**
     * @var int[]
     *   [NUMBER => NO COME UPS count from the last time]
     */
    public $rouletteNumbersNoComeUpsCounter;

    protected $combinationCount;
    protected $nonComeUpsThresold;

    public $betsCount = 0;
    public $maxLossDuringBetSeries = 0;
    public $maxDeposit = 0;
    private $currentLoss = 0;

    public $currentBetsCount = 0;
    public $maxBetCountInRow = 0;

    public $maxBetsCount = 0;

    const NUMBERS_ROULETTE_COUNT = 38;

    public function __construct($combinationCount, $nonComeUpsThresold, $maxBetsCount = 0)
    {
        $this->parsedNumbers = $this->getNumbers();

        $this->rouletteNumbersNoComeUpsCounter = array_pad([], self::NUMBERS_ROULETTE_COUNT + 1, 0);
        unset($this->rouletteNumbersNoComeUpsCounter[0]);

        $this->combinationCount = $combinationCount;
        $this->nonComeUpsThresold = $nonComeUpsThresold;

        $this->maxBetsCount = $maxBetsCount;
    }

    private function getNumbers()
    {
        $csv = array_map(
          function ($oneLine) {
              $one = str_getcsv($oneLine);

              return $one[0];
          },
          file('numbers_new.csv')
        );
        array_shift($csv);

        return $csv;
    }

    public function printStat(): void
    {
        print "WINNER: $this->combinationCount -> $this->nonComeUpsThresold: ";
        print_r($this->depositWin);
        print "$";
        print PHP_EOL;
        print "Max win: $this->maxDeposit$";
        print PHP_EOL;
        print "Mean: ".$this->depositWin / $this->betsCount;
        print ", Number of bets: $this->betsCount, Max bets in a row: $this->maxBetCountInRow";
        print PHP_EOL;
        print "Max loss: ".$this->maxLossDuringBetSeries;
        print PHP_EOL;
        print PHP_EOL;
    }

    public function writeStatToFile(): void
    {
        if (!file_exists('stat.csv')) {
            $this->writeStatHeader();
        }

        $fp = fopen('stat.csv', 'a');
        fputcsv(
          $fp,
          [
            $this->combinationCount,
            $this->nonComeUpsThresold,
            $this->depositWin / $this->betsCount,
            $this->betsCount,
            $this->maxLossDuringBetSeries,
            $this->depositWin,
          ]
        );
    }

    private function writeStatHeader(): void
    {
        $fp = fopen('stat.csv', 'a');
        fputcsv(
          $fp,
          [
            'Комбинация',
            'Не выпадения',
            'Средний выигрышь',
            'Количество ставок',
            'Максимальный проигрышь',
            'Выигрышь',
          ]
        );
        fclose($fp);
    }

    public function play()
    {
        for ($step = 0; $step < count($this->parsedNumbers); $step++) {
            $this->playCurrentRound($step);
        }
    }

    private function playCurrentRound($currentGameRound)
    {
        $this->makeABet($currentGameRound);

        $this->updateNoComeUpCounters($this->getNumber($currentGameRound));
    }

    private function getNumber($step = 0)
    {
        return $this->parsedNumbers[$step] ?? false;
    }

    private function makeABet($currentGameRound)
    {
        $nonComeUpNumbersCombination = $this->getNonComeUpsCombination();

        $combinationIsActive = count($nonComeUpNumbersCombination) >= $this->combinationCount;
        if ($combinationIsActive) {
            $this->depositWin -= count($nonComeUpNumbersCombination);
            $this->currentLoss -= count($nonComeUpNumbersCombination);
            $this->currentBetsCount++;
            $this->betsCount++;
        }

        $numberIsInCombination = in_array($this->getNumber($currentGameRound), $nonComeUpNumbersCombination);
        if ($combinationIsActive && $numberIsInCombination) {
            $this->depositWin += 36;
            $this->maxLossDuringBetSeries = min($this->currentLoss, $this->maxLossDuringBetSeries);
            $this->maxBetCountInRow = max($this->currentBetsCount, $this->maxBetCountInRow);
            $this->currentLoss = 0;
            $this->currentBetsCount = 0;
            $this->maxDeposit = max($this->depositWin, $this->maxDeposit);

            $this->zeroNonComeUpCounters();
        } elseif ($this->maxBetsCount && $this->currentBetsCount >= $this->maxBetsCount) {
            $this->zeroNonComeUpCounters();
            $this->maxLossDuringBetSeries = min($this->currentLoss, $this->maxLossDuringBetSeries);
            $this->maxBetCountInRow = max($this->currentBetsCount, $this->maxBetCountInRow);
            $this->currentLoss = 0;
            $this->currentBetsCount = 0;
        }
    }

    private function getNonComeUpsCombination()
    {
        $nonComeUpNumbersCombination = [];

        foreach ($this->rouletteNumbersNoComeUpsCounter as $number => $oneNumberCounter) {
            if ($oneNumberCounter >= $this->nonComeUpsThresold) {
                $nonComeUpNumbersCombination[] = $number;
            }
        }

        return $nonComeUpNumbersCombination;
    }

    private function zeroNonComeUpCounters(): void
    {
        foreach ($this->rouletteNumbersNoComeUpsCounter as &$oneNumberCounter) {
            $oneNumberCounter = 0;
        }
    }

    private function updateNoComeUpCounters($currentRoundNumber): void
    {
        foreach ($this->rouletteNumbersNoComeUpsCounter as $number => &$oneNumberCounter) {
            if ($currentRoundNumber == $number) {
                $oneNumberCounter = 0;
            } else {
                $oneNumberCounter++;
            }
        }
    }
}

//$strategy = new Strategy(1, 79);
//$strategy->play();
//$strategy->printStat();
//die();

for ($combination = 1; $combination < 37; $combination++) {
    for ($nonComeUpsThresold = 1; $nonComeUpsThresold < 100; $nonComeUpsThresold++) {
        $strategy = new Strategy($combination, $nonComeUpsThresold, 3);
        $strategy->play();

        if ($strategy->depositWin >= 0 && $strategy->betsCount >= 100) {
            $strategy->printStat();
            $strategy->writeStatToFile();
        }
    }
}

