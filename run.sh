#!/bin/bash

set -e

cd /var/app

[ ! -d "node_modules" ] && npm install
[ ! -d "build" ] && npx tsc

exec "$@"