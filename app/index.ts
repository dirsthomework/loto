import HeadlessLotoBrowserTaker from "./src/HeadlessLotoBrowserTaker";
import App from "./src/App";
import * as fs from "fs";

(async () => {
    const lotoBrowser = new HeadlessLotoBrowserTaker();
    const page = await lotoBrowser.gotoLotoPage();
    const app = new App();

    let lastNumbers = fs.existsSync('numbers.csv') ? await app.readCsvNumbers(App.historyNumbersCount, 'numbers.csv') : [];
    let historyNumbersOnImage: Array<number> = [];
    let attemptForSameResultCounter = 0;

    await lotoBrowser.extractImages();
    await app.waitForLoad(lotoBrowser);

    while (true) {
        await lotoBrowser.extractImages();

        try {
            historyNumbersOnImage = await app.readAllHistoryByScreenshot();

            if (historyNumbersOnImage.toString() === lastNumbers.toString()) {
                console.log('no new numbers...');
                attemptForSameResultCounter++;
            } else {
                console.log('write new numbers...');
                attemptForSameResultCounter = 0;
                await app.writeAllNewNumbers(historyNumbersOnImage, lastNumbers);
            }

        } catch (exception) {
            // @ts-ignore
            console.log(exception.message);
            continue;
        }

        if (attemptForSameResultCounter > 50) {
            break;
        }

        lastNumbers = historyNumbersOnImage;
    }

    await lotoBrowser.browser.close();
})()