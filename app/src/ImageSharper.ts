import {ImageLocationsEnum} from "./Types/ImageLocationsEnum";

const sharp = require("sharp");
sharp.cache(false);

export default class ImageSharper {
    public async cutMainNumber(): Promise<void> {
        await sharp(ImageLocationsEnum.mainScreenshot).extract({
            width: 126, height: 103, left: 267, top: 99
        }).toFile(ImageLocationsEnum.cutNumberScreenshot);
    }

    public async cutHistoryNumber(): Promise<void> {
        await sharp(ImageLocationsEnum.mainScreenshot).extract({
            width: 56, height: 54, left: 250, top: 229
        }).toFile(ImageLocationsEnum.cutNumberHistoryScreenshot);
    }

    public async cutProgress(): Promise<void> {
        await sharp(ImageLocationsEnum.mainScreenshot).extract({
            width: 68, height: 29, left: 171, top: 303
        }).toFile(ImageLocationsEnum.progressImage);
    }

    public async cutHistoryNumberOnPosition(position: number): Promise<void> {
        const height = 54;

        let body = 0;
        if (position > 3 && position < 10) {
            body = position - 2;
        } else if (position >= 10) {
            body = position - 3;
        } else {
            body = position - 1;
        }


        const dynamicOffsetTop = 229 + 56 * (position - 1) + body;

        await sharp(ImageLocationsEnum.mainScreenshot).extract({
            width: 56, height: height, left: 250, top: dynamicOffsetTop
        }).toFile(ImageLocationsEnum.cutNumberHistoryScreenshot);
    }
}