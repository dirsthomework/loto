import ImageHasher from "./ImageHasher";
import {ImageLocationsEnum} from "./Types/ImageLocationsEnum";

const Tesseract = require("tesseract.js");

export default class ImageRecognizer {

    public async recognizeCurrentMainNumber(): Promise<number> {
        const imageHasher = new ImageHasher();

        return imageHasher.getNumberForImage(ImageLocationsEnum.cutNumberScreenshot);
    }

    public async recognizeCurrentHistoryNumber(): Promise<number> {
        const imageHasher = new ImageHasher();

        return imageHasher.getNumberForHistoryImage(ImageLocationsEnum.cutNumberHistoryScreenshot);
    }

    public async isAwaitingNumber(): Promise<Boolean> {
        const progressRecognized = await this.recognizeRawProgress();

        return !progressRecognized.match(/\d\d:\d\d/i);
    }

    public async isPageLoading(): Promise<boolean> {
        try {
            const imageHasher = new ImageHasher();

            const number = await imageHasher.getNumberForImage(ImageLocationsEnum.cutNumberScreenshot);

            return number <= ImageHasher.screenIsLoadingHashNumber;
        } catch {
            return false;
        }
    }

    public async getSecondsTillNextGame(): Promise<number> {
        const progressRecognized = await this.recognizeRawProgress();

        const secondsMatch = progressRecognized.match(/(?<=:)\d\d/i);
        if (secondsMatch) {
            return parseInt(secondsMatch[0]);
        }

        if (await this.isAwaitingNumber()) {
            return -1;
        }

        throw new Error('Can\'t recognize progress ' + progressRecognized);
    }

    private async recognizeRawProgress(): Promise<string> {
        let progressRecognized = await Tesseract.recognize(ImageLocationsEnum.progressImage, "eng");

        return progressRecognized.data.text;
    }
}