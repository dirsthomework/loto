import * as fs from "fs";
import * as imageHash from "node-image-hash";
import {ImageLocationsEnum} from "./Types/ImageLocationsEnum";
const compareImages = require("resemblejs/compareImages");

export default class ImageHasher {

    public static hashCache: HashCollectionItem[] = [];

    public static screenIsLoadingHashNumber: number = 0;

    public async readAllImagesToHashes(): Promise<HashCollectionItem[]> {
        if (ImageHasher.hashCache.length) {
            return ImageHasher.hashCache;
        }

        const filesNames = fs.readdirSync(ImageLocationsEnum.numbersFolders);
        let hashes: HashCollectionItem[] = [];

        for (const fileName of filesNames) {
            if (fileName.match(/_small/)) {
                continue;
            }

            let hash = await this.getHashCollectionItemByImagePath(
                parseInt(fileName),
                ImageLocationsEnum.numbersFolders + '/' + fileName
            );

            hashes.push(hash);
        }

        ImageHasher.hashCache = hashes;

        return hashes;
    }

    public async getHashCollectionItemByImagePath(number: number, numberImagePath: string): Promise<HashCollectionItem> {
        const historyNumberPath = numberImagePath.replace(ImageLocationsEnum.extension, '_small' + ImageLocationsEnum.extension);

        return {
            number: number,
            mainImageHash: (await imageHash.syncHash(numberImagePath)).hash,
            historyImageHash: fs.existsSync(historyNumberPath) ? (await imageHash.syncHash(historyNumberPath)).hash : ''
        };
    }

    public async getHashForNumber(number: number): Promise<HashCollectionItem> {
        const allHashes = await this.readAllImagesToHashes();

        for (const hash of allHashes) {
            if (hash.number === number) {
                return hash;
            }
        }

        throw new Error('Hash wasn\'t found for' + number + ' in ' + allHashes);
    }

    public async getNumberForHistoryImage(imagePath: string): Promise<number> {
        const filesNames = fs.readdirSync(ImageLocationsEnum.numbersFolders);
        for (const fileName of filesNames) {
            if (!fileName.match(/_small/)) {
                continue;
            }

            let numberCurrent = parseInt(fileName.replace('_small', '').replace('.png', ''));

            let numberImageCurrent = ImageLocationsEnum.numbersFolders + '/' + fileName;

            const compared = await compareImages(
                imagePath,
                numberImageCurrent,
                {ignore: 'antialiasing'}
            );

            if (parseFloat(compared.misMatchPercentage) <= 1) {
                return numberCurrent;
            }

        }

        throw new Error('Number not found for history image');
    }

    public async getNumberForImage(imagePath: string): Promise<number> {
        const imageHash = await this.getImageHash(imagePath);

        for (const hash of await this.readAllImagesToHashes()) {
            if (hash.mainImageHash === imageHash) {
                return hash.number;
            }
        }

        throw new Error('Hash wasn\'t found for main image');
    }

    public async getImageHash(imagePath: string): Promise<string> {
        return (await imageHash.syncHash(imagePath)).hash
    }

}