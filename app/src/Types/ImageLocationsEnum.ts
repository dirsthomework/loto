export enum ImageLocationsEnum {
    numbersFolders = 'screenshots/numbers',
    mainScreenshot = 'screenshots/loto.png',
    cutNumberScreenshot = 'screenshots/lotoNumber.png',
    cutNumberHistoryScreenshot = 'screenshots/lotoNumberSmall.png',
    progressImage = 'screenshots/progress.png',

    extension = '.png'
}