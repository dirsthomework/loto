declare type HashCollectionItem = {
    number: number,
    mainImageHash: string,
    historyImageHash: string
}