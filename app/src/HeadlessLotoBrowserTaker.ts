import puppeteer from "puppeteer-extra";
import {Browser, Page} from "puppeteer";
import {ImageLocationsEnum} from "./Types/ImageLocationsEnum";
import ImageSharper from "./ImageSharper";

export default class HeadlessLotoBrowserTaker {
    private viewPortWidth: number = 1980;
    private viewPortHeight: number = 1024;
    private pageAddress: string = 'https://betboom.ru/game/tennis38';
    public sharper: ImageSharper = new ImageSharper();
    public browser!: Browser;

    private browserPageLoaded!: Page;

    constructor() {
        const StealthPlugin = require('puppeteer-extra-plugin-stealth')
        puppeteer.use(StealthPlugin());
    }

    public async gotoLotoPage(): Promise<Page> {

        this.browser = await puppeteer.launch({
            headless: true,
            args: [
                '--no-sandbox',
                '--disable-setuid-sandbox',
            ]
        });

        this.browserPageLoaded = await this.browser.newPage();
        await this.browserPageLoaded.setViewport({
            width: this.viewPortWidth,
            height: this.viewPortHeight,
            deviceScaleFactor: 1,
        });

        await this.browserPageLoaded.goto(this.pageAddress);

        return this.browserPageLoaded;
    }

    public async extractImages(): Promise<void> {
        if (!this.browserPageLoaded) {
            await this.gotoLotoPage();
        }

        await this.browserPageLoaded.screenshot({path: ImageLocationsEnum.mainScreenshot});

        await this.sharper.cutMainNumber();
        await this.sharper.cutHistoryNumber();
        await this.sharper.cutProgress();
    }
}