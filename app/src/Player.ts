import * as fs from "fs";
import {parse} from "csv-parse/lib/sync";

export default class Player {

    private parsedNumbers: Array<number> = [];

    private rouletteNumbersNoComeUpsCounter: Array<NonComeupCounter> = [];

    private NUMBERS_ROULETTE_COUNT = 38;

    protected combinationCount: number = 0;
    protected nonComeUpsThresold: number = 0;

    protected depositWin = 0;

    public async construct(combinationCount: number, nonComeUpsThresold: number) {
        this.parsedNumbers = await this.readAllCsvNumbers('numbers.csv');

        for (let oneNumber = 1; oneNumber <= this.NUMBERS_ROULETTE_COUNT; oneNumber++) {
            this.rouletteNumbersNoComeUpsCounter.push({number: oneNumber, counter: 0})
        }

        this.combinationCount = combinationCount;
        this.nonComeUpsThresold = nonComeUpsThresold;
    }

    private async readAllCsvNumbers(csvPath: string): Promise<Array<number>> {
        const csvContent = fs.readFileSync(csvPath);

        let parsed = parse(csvContent, {
            columns: true,
            skip_empty_lines: true
        });

        let lastNumbers = [];

        for (const oneLine of parsed) {
            // @ts-ignore
            lastNumbers.push(oneLine.number);
        }

        return lastNumbers.reverse();
    }


    public async play() {

        for (let step = 0; step < this.parsedNumbers.length; step++) {
            this.readDeposit();

            await this.playCurrentRound(step);

            this.writeDeposit();
        }
    }

    private readDeposit(): void {
        if (fs.existsSync('deposit')) {
            this.depositWin = <number><unknown>fs.readFileSync('deposit').toString();
        }
    }

    private writeDeposit(): void {
        fs.writeFileSync('deposit', <string><unknown>this.depositWin)
    }

    private async playCurrentRound(currentGameRound: number) {
        await this.makeABet(currentGameRound);

        this.updateNoComeUpCounters(this.getNumber(currentGameRound));
    }

    private makeABet(currentGameRound: number) {
        let nonComeUpNumbersCombination: Array<number> = this.getNonComeUpsCombination();

        let combinationIsActive: boolean = nonComeUpNumbersCombination.length >= this.combinationCount;
        if (combinationIsActive) {
            this.depositWin -= nonComeUpNumbersCombination.length;
        }

        let numberIsInCombination: boolean = nonComeUpNumbersCombination.includes(this.getNumber(currentGameRound));
        if (combinationIsActive && numberIsInCombination) {
            this.depositWin += 36;

            this.zeroNonComeUpCounters();
        }
    }

    private getNumber(step: number = 0) {
        return this.parsedNumbers[step] ?? false;
    }

    private getNonComeUpsCombination(): Array<number> {
        let nonComeUpNumbersCombination: Array<number> = [];

        for (let oneCounter of this.rouletteNumbersNoComeUpsCounter) {
            if (oneCounter.counter >= this.nonComeUpsThresold) {
                nonComeUpNumbersCombination.push(oneCounter.number);
            }
        }

        return nonComeUpNumbersCombination;
    }

    private zeroNonComeUpCounters(): void {
        for (let oneCounter of this.rouletteNumbersNoComeUpsCounter) {
            oneCounter.counter = 0;
        }
    }

    private updateNoComeUpCounters(currentRoundNumber): void {
        for (let oneCounter of this.rouletteNumbersNoComeUpsCounter) {
            if (currentRoundNumber == oneCounter.number) {
                oneCounter.counter = 0;
            } else {
                oneCounter.counter++;
            }
        }
    }

}