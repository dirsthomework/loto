import ImageRecognizer from "./ImageRecognizer";
import * as ObjectsToCsv from 'objects-to-csv';
import {parse} from 'csv-parse/sync';
import * as fs from "fs";
import ImageSharper from "./ImageSharper";
import HeadlessLotoBrowserTaker from "./HeadlessLotoBrowserTaker";

const ProgressBar = require('progress');

export default class App {
    public static historyNumbersCount = 14;

    private loadingRepeatsConst: number = 20;

    public recognizer: ImageRecognizer = new ImageRecognizer();
    public sharper: ImageSharper = new ImageSharper();

    public async readCsvNumbers(readCount: number, csvPath: string): Promise<Array<number>> {
        const csvContent = fs.readFileSync(csvPath);

        let parsed = parse(csvContent, {
            columns: true,
            skip_empty_lines: true
        });

        parsed = parsed.slice(-1 * readCount);

        let lastNumbers = [];

        for (const oneLine of parsed) {
            // @ts-ignore
            lastNumbers.push(oneLine.number);
        }

        return lastNumbers.reverse();
    }

    public async readAllHistoryByScreenshot(): Promise<Array<number>> {
        let history: Array<number> = [];

        for (let pos = 1; pos <= App.historyNumbersCount; pos++) {
            await this.sharper.cutHistoryNumberOnPosition(pos);

            let number = await this.recognizer.recognizeCurrentHistoryNumber();
            history.push(number);
        }

        return history;
    }

    public async writeAllNewNumbers(newNumbers: Array<number>, oldNumbers: Array<number>): Promise<void> {
        let numbersToWrite: Array<number> = [];
        let newNumbersClone = [...newNumbers];
        let oldNumbersClone = [...oldNumbers];

        while (newNumbers.length) {
            if (newNumbersClone.toString() === oldNumbersClone.toString()) {
                break;
            }

            let newNumber = newNumbersClone.shift();
            oldNumbersClone.pop();

            newNumber && numbersToWrite.push(newNumber);
        }

        for (const number of numbersToWrite.reverse()) {
            await this.saveNumberToFile(number);
        }
    }

    public async waitForLoad(browser: HeadlessLotoBrowserTaker): Promise<void> {
        console.log('loading...')

        let loadingRepeats = 0;

        while (await this.recognizer.isPageLoading() && loadingRepeats < this.loadingRepeatsConst) {
            this.renderTimer(5);
            await this.sleep(5);

            loadingRepeats++;

            await browser.extractImages();
        }

        console.log('page is loaded...')
    }

    private async saveNumberToFile(number: number): Promise<void> {
        const csv = new ObjectsToCsv([{
            number: number,
            date: new Date()
        }]);

        await csv.toDisk('./numbers.csv', {append: true})
    }

    private async extractNumber(): Promise<number> {
        return await this.recognizer.recognizeCurrentHistoryNumber();
    }

    public async renderTimer(seconds: number): Promise<void> {
        var timerSeconds = seconds;

        if (!seconds) {
            return;
        }

        let bar = new ProgressBar('Waiting :bar', {total: timerSeconds})
        var interval = setInterval(function () {
            bar.tick();

            if (bar.complete) {
                clearInterval(interval);
            }
        }, 1000);
    }

    public async sleep(seconds: number): Promise<void> {
        await new Promise(
            resolve => setTimeout(
                resolve,
                seconds * 1000
            )
        );
    }
}