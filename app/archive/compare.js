const util = require('util')
const looksSame = util.promisify(require('looks-same'));
const resemble = require('resemblejs');
const compareImages = require("resemblejs/compareImages");
const Jimp = require("jimp");

(async () => {
    //
    // let image = await Jimp.read('screenshots/lotoNumberSmall.png');
    // await image.grayscale()
    // await image.writeAsync('screenshots/compare1.png');
    //
    // image = await Jimp.read('screenshots/numbers/28_small.png');
    // await image.grayscale();
    // await image.writeAsync('screenshots/compare2.png');

    const data = await compareImages(
        'screenshots/lotoNumberSmall.png',
        'screenshots/numbers/15_small.png',
        {ignore: 'antialiasing'}
    );

    // const a = await resemble('screenshots/lotoNumberSmall.png')
    //     .compareTo('screenshots/numbers/2_small.png')
    //     .ignoreColors();

    console.log(data);

    // const l = await looksSame(
    //     'screenshots/lotoNumberSmall.png',
    //     'screenshots/numbers/15_small.png',
    //     {tolerance: 12}
    // );
    // console.log(l);
})();
