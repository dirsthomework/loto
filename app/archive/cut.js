const sharp = require("sharp");
const fs = require("fs");
sharp.cache(false);

(async () => {

    const filesNames = fs.readdirSync('screenshots/numbers');

    for (const fileName of filesNames) {
        if (!fileName.match(/_small/)) {
            continue;
        }
        //
        // console.log(fileName);

        await sharp('screenshots/numbers/' + fileName).extract({
            width: 56, height: 54, left: 1, top: 0
        }).toFile('screenshots/numbers/n/' + fileName);
    }

})();