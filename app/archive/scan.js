const puppeteer = require('puppeteer');
const sharp = require('sharp');
const tesseract = require("tesseract.js");
const Jimp = require('jimp');
const ObjectsToCsv = require('objects-to-csv')
const Tesseract = require("tesseract.js");
const fs = require('fs');

const lotoImagePath = 'screenshots/loto.png';
const lotoImageNumberPath = 'screenshots/lotoNumber.png';
const lotoImageProgressPath = 'screenshots/progress.png';

const isInteger = num => /^[0-9]+$/.test(num + '');

sharp.cache(false);

var firstRoll = true;

(async () => {
    const browser = await puppeteer.launch({
        headless: true,
        args: [
            '--no-sandbox',
            '--disable-setuid-sandbox',
        ]
    });
    const page = await browser.newPage();
    page.setViewport({
        width: 1980,
        height: 1024,
        deviceScaleFactor: 1,
    });
    await page.goto('https://betboom.ru/game/tennis38');

    while (true) {
        await page.screenshot({path: lotoImagePath});

        await sharp(lotoImagePath).extract({
            width: 126, height: 103, left: 267, top: 99
        }).toFile(lotoImageNumberPath);

        await sharp(lotoImagePath).extract({
            width: 68, height: 29, left: 171, top: 303
        }).toFile(lotoImageProgressPath);

        let image = await Jimp.read(lotoImageNumberPath);
        await image.posterize(1);
        await image.writeAsync(lotoImageNumberPath);

        console.log('progress recognition...');
        let progressRecognized = await tesseract.recognize(lotoImageProgressPath, "eng");

        if (progressRecognized.data.text.match(/\d\d:\d\d/i)) {
            console.log(progressRecognized.data.text);

            // if timer is 0 it means there are issues on the page - reload.
            if (progressRecognized.data.text.match(/00:00/)) {
                break;
            }

            if (!firstRoll) {
                console.log('number recognition...');
                let worker = await tesseract.createWorker();
                await worker.load();
                await worker.loadLanguage('eng');
                await worker.initialize('eng');
                await worker.setParameters({
                    tessedit_char_whitelist: '0123456789',
                });
                let recognizedNumber = await worker.recognize(lotoImageNumberPath);
                await worker.terminate();

                // let recognizedNumber = await worker.recognize(lotoImageNumberPath, 'eng');
                console.log(recognizedNumber.data.text);

                // if number is not recognized but timer exists, this might mean we need to refresh page to remove animation issues.
                if (!recognizedNumber.data.text.match(/[0-9]+/i)) {
                    break;
                }

                await sharp(lotoImagePath).extract({
                    width: 126, height: 103, left: 267, top: 99
                }).toFile('screenshots/numbers/' + recognizedNumber.data.text.trim() + '.png');

                await sharp(lotoImagePath).extract({
                    width: 57, height: 54, left: 249, top: 229
                }).toFile('screenshots/numbers/' + recognizedNumber.data.text.trim() + '_small.png');

            }

            let secondsToWait = progressRecognized.data.text.match(/(?<=:)\d\d/i)[0];
            console.log('Waiting for ' + (2 + parseInt(secondsToWait)) + ' sec');

            await new Promise(
                r => setTimeout(r, 2000 + 1000 * parseInt(secondsToWait))
            );

            firstRoll = false;
        }


    }

    await browser.close();
})();