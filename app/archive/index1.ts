import HeadlessLotoBrowserTaker from "../src/HeadlessLotoBrowserTaker";
import App from "../src/App";
import ImageHasher from "../src/ImageHasher";

(async () => {
    // Update hashes cache.
    (new ImageHasher()).readAllImagesToHashes();

    const lotoBrowser = new HeadlessLotoBrowserTaker();
    const page = await lotoBrowser.gotoLotoPage();
    const app = new App();

    while (true) {
        console.log('start new extraction');

        try {
            await lotoBrowser.extractImages(page);

            await app.saveNumber(page);

            await lotoBrowser.extractImages(page);

            await app.waitForNextGame();
        } catch (exception) {
            console.log(exception instanceof Error ? exception.message : '');

            continue;
        }

        console.log('check timer bug...')
        await lotoBrowser.extractImages(page);

        if (await app.isTimerFrozeBug()) {
            break;
        }
    }

    await lotoBrowser.browser.close();
})();