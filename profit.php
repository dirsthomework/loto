<?php

$numbers = range(1, 38);

$kef = 36;
$commonProfit = 0;
$numberProfit = [];

$csv = array_map(
  function ($oneLine) {
      $one = str_getcsv($oneLine);

      return $one[0];
  },
  file('numbers.csv')
);
array_shift($csv);

$noAppear = 0;

foreach ($numbers as $number) {
    $numberProfit[$number] = 0;
    $noAppear = 0;
//    if ($number != 37) {
//        continue;
//    }

    foreach ($csv as $position => $csvNumber) {
        if ($number != $csvNumber) {
            if ($noAppear > 70 && $noAppear <= 85) {
                $numberProfit[$number]--;
                $commonProfit--;
            }

            $noAppear++;
        } elseif ($noAppear > 70 && $noAppear <= 85) {
            $numberProfit[$number]--;
            $commonProfit--;

            $commonProfit += 36;
            $numberProfit[$number] += 36;
            $noAppear = 1;
        } elseif ($noAppear <= 70 || $noAppear > 85) {
//            print "no appear: $noAppear".PHP_EOL;
//            print "Position: ".($position + 2).PHP_EOL;
//            print "Profit ". $commonProfit . PHP_EOL;
            $noAppear = 1;
        }
    }
}


foreach ($numberProfit as $number => $profit) {
    print "for $number current profit: $profit".PHP_EOL;
}
print "Common profit: $commonProfit".PHP_EOL;